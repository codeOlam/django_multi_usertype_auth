from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    
    def create_user(self, email, password, **extra_fields):
        """
        Creates and saves user
        """
        if not email:
        	raise ValueError('Must provide a valid email address')

        now = timezone.now()
        user    = self.model(
                        email=email,
                        date_joined=now,
                        last_login=now,
                        **extra_fields
                            ) 
        
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    
    def create_superuser(self, email, password):
        """
        Creates and saves superuser
        """
        user = self.model(
                          username= username,
                          email = email,                         
                          )
        user.set_password(password)
        user.is_admin =True
        user.save(using=self._db)
        return user


class CustomUserModel(AbstractBaseUser):
    u_id 		    = models.AutoField(primary_key=True, blank=True)      
    email 			= models.EmailField(max_length=127, unique=True, null=False, blank=False)
    is_admin 		= models.BooleanField(default=False)
    is_active 		= models.BooleanField(default=True)
    

    objects = CustomUserManager()

    USERNAME_FIELD = "username"

    REQUIRED_FIELDS = ['email']

    class Meta:
        app_label = "accounts"
        db_table = "users"

    def __str__(self):
        return self.username

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def has_perm(self, perm, obj=None):
        #User has a specific permission
        return True

    def has_module_perms(self, app_label):
        #User have permission to view app label
        return True

    @property
    def is_staff(self):
        #Check if user is staff member
        return self.is_admin
